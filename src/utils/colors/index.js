export const colors = {
  default: '#a55eea',
  disable: '#A5B1C2',
  dark: '#474747',
  text: {
    default: '#7e7e7e',
  },
};
