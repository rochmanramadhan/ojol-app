import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {IconBack, IllustrationRegister} from '../../assets';
import {Button, Input} from '../../components';
import {colors} from '../../utils';

const Register = () => {
  const [form, setForm] = useState({
    fullName: '',
    email: '',
    password: '',
  });

  const sendData = () => {
    console.log('data : ', form);
  };

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    });
  };
  return (
    <View>
      <View style={styles.wrapper}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <IconBack width={25} height={25} />
          <View style={styles.illustration} />
          <IllustrationRegister
            width={199}
            height={150}
            style={styles.illustration}
          />
          <Text style={styles.text.desc}>
            Mohon mengisi beberapa data untuk proses daftar Anda
          </Text>
          <View style={styles.space(64)} />
          <Input
            placeholder="nama lengkap"
            value={form.fullName}
            onChangeText={(value) => onInputChange(value, 'fullName')}
          />
          <View style={styles.space(33)} />
          <Input
            placeholder="email"
            value={form.email}
            onChangeText={(value) => onInputChange(value, 'email')}
          />
          <View style={styles.space(33)} />
          <Input
            placeholder="password"
            value={form.password}
            onChangeText={(value) => onInputChange(value, 'password')}
            secureTextEntry={true}
          />
          <View style={styles.space(83)} />
          <Button title="Daftar" onPress={sendData} />
        </ScrollView>
      </View>
    </View>
  );
};

export default Register;

const styles = {
  wrapper: {padding: 20},
  illustration: {
    marginTop: 8,
  },
  text: {
    desc: {
      fontSize: 14,
      fontWeight: 'bold',
      color: colors.default,
      marginTop: 16,
      maxWidth: 200,
    },
  },
  space: (value) => {
    return {
      height: value,
    };
  },
};
