import React from 'react';
import {Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {colors} from '../../../utils';

const Button = ({title, onPress}) => {
  return (
    <TouchableOpacity
      style={styles.wrapper.component}
      onPress={onPress}>
      <Text style={styles.text.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = {
  wrapper: {
    component: {backgroundColor: colors.default, borderRadius: 25},
  },
  text: {
    title: {
      fontSize: 12,
      color: 'white',
      textTransform: 'uppercase',
      textAlign: 'center',
      fontWeight: 'bold',
      paddingVertical: 13,
    },
  },
};

export default Button;
