// Illustration
import IllustrationWelcomeAuth from './Illustrations/illustration_welcome_auth.svg';
import IllustrationRegister from './Illustrations/illustration_register.svg';
import IllustrationLogin from './Illustrations/illustration_login.svg';

// Icon
import IconBack from './icon/back.svg';

export {
  IllustrationLogin,
  IllustrationRegister,
  IllustrationWelcomeAuth,
  IconBack,
};
